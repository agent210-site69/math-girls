<?php foreach($girlPagesAllArray as $girlPagesArray): ?>
	<div class="container">
		<?php foreach($girlPagesArray as $p): ?>
			<div class="container3">
				<div class="container4">
					<div class="card card1" style="background-image: url('<?= $p->image->url ?>')"></div>
					<div class="date"><?= $p->date ?></div>
					<div class="squares-container">
						<div class="black-square"></div>
						<div class="black-square"></div>
						<div class="black-square"></div>
					</div>
					<div class="lorem-ipsum"><?= $p->math_text ?></div>
				</div>
				<div class="square-container">
					<div class="square">
						<div class="main-circle-container">
							<div class="circle-container">
								<div class="circle"></div>
							</div>
							<div class="circle-container">
								<div class="circle"></div>
							</div>
						</div>
						<div class="semicircle-container">
							<div class="semicircle"></div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>	
<?php endforeach; ?>
