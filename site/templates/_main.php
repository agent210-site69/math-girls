<?php namespace ProcessWire;

// Optional main output file, called after rendering page’s template file. 
// This is defined by $config->appendTemplateFile in /site/config.php, and
// is typically used to define and output markup common among most pages.
// 	
// When the Markup Regions feature is used, template files can prepend, append,
// replace or delete any element defined here that has an "id" attribute. 
// https://processwire.com/docs/front-end/output/markup-regions/
	
/** @var Page $page */
/** @var Pages $pages */
/** @var Config $config */

$home = $pages->get('/'); // homepage

?><!DOCTYPE html>
<html lang="en">
	<head id="html-head">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?php echo $page->title; ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/uikit.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/uikit-rtl.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/main.css" />
	</head>
	<body id="html-body">
		
		<?= wireRenderFile('home.php') ?>
		
		<script src="<?php echo $config->urls->templates; ?>scripts/uikit.js"></script>
		<script src="<?php echo $config->urls->templates; ?>scripts/uikit-icons.js"></script>
		<script src="<?php echo $config->urls->templates; ?>scripts/jquery.min.js"></script>
		<script src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
	</body>
</html>
