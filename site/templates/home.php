<?php namespace ProcessWire;

// Template file for “home” template used by the homepage

$girlPagesAll = $pages->find('template=girl-with-text,sort=-date');

$girlPagesAllArray = [];

for($i = 0; $i < $girlPagesAll->count() / 4; $i++) {
	$girlPagesAllArray[] = $pages->find("template=girl-with-text,start=" . $i*4 . ",limit=4");
}

$vars = ['girlPagesAllArray' => $girlPagesAllArray];

echo wireRenderFile('views/home-view.php', $vars);

?>
